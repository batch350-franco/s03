1. List the books Authored by Marjorie Green.
	au_id: 213-46-8915

	- The Busy Executive's Database Guide (title_id: BU1032)
	- You Can Combat Computer Stress (title_id: BU2075)

2. List the books Authored by Michael O'Leary.
	au_id: 267-41-2394

	- Cooking with Computers (title_id: BU1111)

3. Write the author/s of "The Busy Executive’s Database Guide".
	title_id: BU1032

	- Marjorie Green (au_id: 213-46-8915)
	- Abraham Bennet (au_id: 409-56-7008)

4. Identify the publisher of "But Is It User Friendly?".
	title_id: PC1035

	Algodata Infosystems (pub_id: 1389)

5. List the books published by Algodata Infosystems.
	pub_id: 1389

	- The Busy Executive's Database Guide (title_id: BU1032)
	- Cooking with Computers (title_id: BU1111)
	- Straight Talk About Computers (title_id: BU7832)
	- But Is It User Friendly? (title_id: PC1035)
	- Secrets of Silicon Valley (title_id: PC8888)
	- Net Etiquette (title_id: PC9999)
