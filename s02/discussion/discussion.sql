-- Connection to MySQL via Terminal: mysql -u root
	-- "-u" stands for username
	-- "root" is the default username for sql.
	-- "-p" stands for password
	-- An empty values is the default password for SQL.

-- List down the databases inside the DBMS.
SHOW DATABASES;

-- Note:
	-- SQL QUERIES is case insentive, but to easily identify the queries we usually use UPPERCASE.
	-- Make sure semi-colons (;) are added at the end of the sql syntax.

-- Create a database
CREATE DATABASE music_db;

-- Note: Naming convention in SQL Database and table uses the snake case.

-- Remove a specific database
DROP DATABASE music_db;

-- select the database.
USE music_db;

-- Create tables.
-- Table columns have the following format: 
	-- [column_name] [data_type] [other_options]
	-- "id INT NOT NULL AUTO_INCREMENT": "Integer" value that cannot be "null" (required) and it will "increment" when a user is created.
	-- "VARCHAR(50)" - Variable Character, The storage size of this datatype is equal to the actual length of the entered strings in bytes.
		-- "50" is used to set the character limit.
	-- PRIMARY KEY (id): Unique identifier of the record ("row" in a relational table)

-- Create user table
CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	address VARCHAR(50),
	PRIMARY KEY (id)
);

-- To show tables under a database
SHOW tables;

-- Describing a table allows us to see the table columns, data_types and extra_options.
DESCRIBE users;

/*
    mini-activity:
        1. create a table for artists
        2. artists should have an id
        3. artists is required to have a name with 50 character limits
        4. assign the primary key to its id
        5. use SHOW TABLES to check if the artists table is created.
        6. send a screenshot of your terminal in the batch space.
*/

-- Create artists table
CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

-- Tables with foreign keys;
-- Applying constraints in a table
-- Syntax
--     CONSTRAINT foreign_key_name
--         FOREIGN KEY (column_name)
--         REFERENCES table_name(id)
--         ON UPDATE ACTION
--         ON DELETE ACTION

-- When to create a constraints (foreign key)?
	-- If the relationship is one-to-one, "primary key" of the parent row is added as "foreign key" of the child row.
	-- If the relationship is one-to-many, "foreign key" column is added in the "many" entity/table.
	-- If the relationship is "many-to-many", linking table is created to contain the "foreign key" for both tables/entities

-- Create albums table

-- DATE and TIME FORMAY in SQL
	-- DATE refers ti YYYY-MM-DD 
	-- TIME refers to HH:MM:SS
	-- DATETIME refers to YYYY-MM-DD HH:MM:SS
	
-- Notes:
	-- CONSTRAINT: use to specify the rules for the data table, this is an optional field, but we use this to identify the FOREIGN KEY.
	-- FOREIGN KEY: is used to prevent actions that will destroy links between tables.
	-- REFERENCES: is a field that refers to the primary key of another table.
	-- ON UPDATE CASCADE: If the parent row is changed, the child row will also reflect that change.
	-- ON DELET RESTRICT: You cant delete a given parent row if a child row exist that is referenced to the value for that parent row.
	-- Creating tables with foreign keys to tables that dont exist will return an error.
	-- Dropping tables with foreign keys will also result to an error if the "ON DELETE" action is set to restrict
	-- ON UPDATE actions - NO ACTION, CASCADE, SET NULL, SET DEFAULT
	-- ON DELETE actions - RESTRICT, SET NULL
CREATE TABLE albums(
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(50) NOT NULL,
	date_released DATE NOT NULL,
	artist_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

/*
    mini-activity
    1. Create a table for songs
    2. Put a required auto increment id
    3. declare a song name with 50 char limit, this should be required
    4. declare a length with the data type time and it should be required
    5. declare a genre with 50 char limit, it should be required
    6. declare an integer as album_id that should be required
    7. create a primary key referring to the id of the songs
    8. create a foreign key constraint and name it fk_songs_album_id
        8.a this should be referred to the album id
        8.b it should have a cascaded update and restricted delete
    9. run create table songs in the terminal.
	10. Excute the SHOW tables to check if the table is created.
    11. Send your output in Space
*/

CREATE TABLE SONGS(
	id INT NOT NULL AUTO_INCREMENT,
	song_name VARCHAR(50) NOT NULL,
	length TIME NOT NULL,
	genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY (album_id) REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- Create playlist table
CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	datetime_created DATETIME NOT NULL,
	user_id INT NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_playlists_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- Create a joining playlists songs table
CREATE TABLE playlists_songs(
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_songs_playlist_id
		FOREIGN KEY (playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_playlists_songs_song_id
		FOREIGN KEY (song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);