INSERT INTO users (email, password, datetime_created) VALUES 
	("johnsmith@gmail.com", "passwordA", 20210101010000),
	("juandelacruz@gmail.com", "passwordB", 20210101020000),
	("janesmith@gmail.com", "passwordC", 20210101030000),
	("mariadelacruz@gmail.com", "passwordD", 20210101040000),
	("johndoe@gmail.com", "passwordE", 20210101050000);

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES
	(1, "First Code", "Hello World!", 20210102010000),
	(1, "Second Code", "Hello Earth!", 20210102020000),
	(2, "Third Code", "Welcome to Mars!", 20210102030000),
	(4, "Fourth Code", "Bye bye solar system!", 20210102040000);

-- 3
SELECT * FROM posts WHERE author_id = 1;

-- 4
SELECT email, datetime_created FROM users;

-- 5
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE content ="Hello Earth!";

-- 6
DELETE FROM users WHERE email = "johndoe@gmail.com";

-- STRETCH GOALS
INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES
	("johndoe", "john1234", "John Doe", 09123456789, "john@mail.com", "Quezon City");

INSERT INTO playlists (datetime_created, user_id) VALUES
	(20230920080000, 1);

INSERT INTO playlists_songs (playlist_id, song_id) VALUES
	(1, 1),(1, 2);
