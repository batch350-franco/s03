-- MySQl CRUD Operation


-- [SECTION] Inserting a Record
-- Syntax:
	-- INSERT INTO table_name (column_name) VALUES (Value1);

INSERT INTO artists (name) VALUES ("Psy");
INSERT INTO artists (name) VALUES ("Rivermaya");

-- Inserting a record with multiple columns.
-- SYNTAX:
	--  INSERT INTO table_name(column_nameA, column_nameB) VALUES (valueA, valueB);

-- Albums Table
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2013-08-15", 1);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-02-14", 2);

-- MySQL non-delimeter Date and Time literals
	-- This allows Date and Time values to be represented without delimeters (such as hypen, colon, or spaces)

	-- Time Format
	  	-- Original: HH:MM:SS
	  	-- Non-delimiter: HHMMSS
	  	-- But make sure that it still follow the correct "max" value for each time intervals:
	  		-- HH = 24
	  		-- MM = 59
	  		-- SS = 59

-- Songs Table
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 339, "K-pop", 1);

-- Mini-Activity:
	-- Insert the following songs in the songs table:

		-- Song name: Kundiman
		-- Length: 5 mins 24 secs
		-- Genre: OPM
		-- Album: Trip

		-- Song name: Kisapmata
		-- Length: 4 mins 41 secs
		-- Genre: OPM
		-- Album: Trip

	-- Check if the songs are added.
	-- send your songs table in the batch space

-- Adding multiple records with one query.
-- SYNTAX:
	-- INSERT INTO table_name (column_name, column_name) VALUES (value1, value2), (valueA, valueB);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kundiman", 524, "OPM", 2), ("Kisapmata", 441, "OPM", 2);

-- [SECTION] Selecting Records

-- Showing all record details
-- SYNTAX:
	-- SELECT * FROM table_name;
		-- (*) means all columns will be shown in the selected table.

SELECT * FROM songs;

-- Show records with selected columns.
-- Syntax:
	-- SELECT column_nameA, column_nameB FROM table_name;

-- Display the title and genre of all songs.
SELECT song_name, genre FROM songs;

-- Display album title and date released of all albums.
SELECT album_title, date_released FROM albums;

-- Show records that meets a certain condition.
--  SYNTAX:
	-- SELECT column_name FROM table_name WHERE condition;
	-- "WHERE" clause is used to filter records and to extract only those records that fulfill a specific condition.

-- Display the title of all the OPM songs;
SELECT song_name FROM songs WHERE genre = "OPM";

-- Mini Activity
-- Display the title of all songs where length is less than four minutes.
SELECT song_name FROM songs WHERE length < 400;

-- Show records with multiple conditions.
-- Syntax:
	-- AND clause
		-- SELECT column_name FROM table_name WHERE condition1 AND condition2
	-- OR clause
		-- SELECT column_name FROM table_name WHERE condition1 OR condition2

-- Display the title and length of the OPM songs that are more than 4 minutes and 30 seconds.
SELECT song_name, length FROM songs WHERE length > 430 AND genre = "OPM";

-- [SECTION] Updating Records

-- Updating single column of a record.
-- Syntax:
	 -- UPDATE table_name SET column_name = new_value WHERE condition;

-- Update the length of Kundiman to 4 minutes 24 secs.
UPDATE songs SET length = 424 WHERE song_name = "Kundiman";

-- Check if the song Kundiman is updated.
SELECT song_name, length FROM songs WHERE song_name = "Kundiman";

-- Updating multiple columns of records.
-- Syntax:
	-- UPDATE table_name SET column_name1 = new_value1, column_name2 = new_value2 WHERE condition;

-- Update the album with a title of Psy 6 and change its title to "Psy 6 (Six Rules)" and date released to July 15, 2012.
UPDATE albums SET album_title = "Psy 6 (Six Rules)", date_released = 20120715 WHERE album_title = "Psy 6";

-- Show the update for Psy 6
SELECT * FROM albums WHERE album_title = "Psy 6";

-- Deleting a record
-- Syntax:
	-- DELETE FROM table_name WHERE condition;
	-- Note: Removing the WHERE clause will remove all rows in the table.

-- Delete all OPM songs that are more than 4 minutes and 30 seconds.
DELETE FROM songs WHERE genre = "OPM" AND length >430;

-- Check if which song is deleted
SELECT * FROM songs;
